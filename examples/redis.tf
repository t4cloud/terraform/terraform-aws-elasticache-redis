module "elasticache_CLUSTER_NAME" {
  #source                     = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-elasticache.git?ref=0.12"
  source                     = "/mnt//dados//git//ganex/public/terraform/terraform-aws-elasticache-redis/"
  name                       = "CLUSTER_NAME"
  node_type                  = "cache.t3.micro"
  engine_version             = "5.0.6"
  cluster_size               = 1
  availability_zones         = data.terraform_remote_state.INFRA.outputs.vpc_INFRA_azs
  automatic_failover_enabled = false
  at_rest_encryption_enabled = true
  transit_encryption_enabled = true
  auth_token                 = "${file(".secrets/elasticache-CLUSTER_NAME-auth-token.txt")}"
  port                       = "6379"
  maintenance_window         = "mon:06:00-mon:07:00"
  snapshot_window            = "04:10-05:10"
  snapshot_retention_limit   = 7
  apply_immediately          = true
  family                     = "redis5.0"
  subnet_ids                 = data.terraform_remote_state.INFRA.outputs.vpc_INFRA_private_subnets
  vpc_id                     = data.terraform_remote_state.INFRA.outputs.vpc_INFRA_vpc_id
  allowed_security_groups    = ["${data.terraform_remote_state.INFRA.outputs.worker_security_group_id}"]
  parameter = [
    {
      name  = "maxmemory-policy"
      value = "allkeys-lru"
    },
  ]
}
