output "id" {
  value       = join("", aws_elasticache_replication_group.default.*.id)
  description = "The ID of the ElastiCache Replication Group."
}

output "security_group_id" {
  value       = join("", aws_security_group.default.*.id)
  description = "Security group ID"
}

output "port" {
  value       = var.port
  description = "Redis port"
}

output "endpoint" {
  value       = var.cluster_mode_enabled ? join("", aws_elasticache_replication_group.default.*.configuration_endpoint_address) : join("", aws_elasticache_replication_group.default.*.primary_endpoint_address)
  description = "Redis primary endpoint"
}

output "member_clusters" {
  value       = flatten(aws_elasticache_replication_group.default.*.member_clusters)
  description = "The identifiers of all the nodes that are part of this replication group."
}
